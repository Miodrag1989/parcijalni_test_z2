﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parcijalni_test_z2
{
    class StudentViseGodine : Student
    {
        private int brojZaostalihIspita;
        private int godinaStudija;

        public StudentViseGodine(string ime, string prezime, float prosecnaOcena, string brojIndeksa, ushort godinaRodjenja,
            int brojZaostalihIspita,int godinaStudija)

            :base(ime,prezime,prosecnaOcena,brojIndeksa,godinaRodjenja)

        {
            this.brojZaostalihIspita = brojZaostalihIspita;
            this.godinaStudija = godinaStudija;
        }

        public override float Bodovi()
        {

            return base.Bodovi() + (3* this.godinaStudija) - (2 * this.brojZaostalihIspita);

        }

        public override void Prikaz()
        {
            Console.WriteLine("Student vise godine:");
            base.Prikaz();
            Console.WriteLine($"Broj zaostalih ispita: {this.brojZaostalihIspita} Godina studija: {this.godinaStudija} je osvojio {this.Bodovi()} bodova.");
        }


    }
}
