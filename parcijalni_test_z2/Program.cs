﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parcijalni_test_z2
{
    class Program
    {
        public static void Ucitaj(out string ime, out string prezime, out float prosecnaOcena, out string brojIndeksa, out ushort godinaRodjenja)
        {
            
            Console.WriteLine("Ime studenta:");
            ime = Console.ReadLine();
            Console.WriteLine("Prezime studenta:");
            prezime = Console.ReadLine();
            Console.WriteLine("Prosecna ocena studenta:");
            float.TryParse(Console.ReadLine(), out prosecnaOcena);
            Console.WriteLine("Broj indeksa studenta:");
            brojIndeksa = Console.ReadLine();
            Console.WriteLine("Godina rodjenja studenta:");
            ushort.TryParse(Console.ReadLine(), out godinaRodjenja);

        }

        public static int NajviseBodova(Student[] nizStudenta)
        {
            int index = 0;
            for(int i = 0; i < nizStudenta.Length; i++)
            {
                if (nizStudenta[index].Bodovi() < nizStudenta[i].Bodovi())
                {
                    index = i;
                }    
                
            }
            return index;
        }
        public static int NajstarijiStudent(Student[] nizStudenta)
        {
            int index = 0;
            for (int i = 0; i < nizStudenta.Length; i++)
            {
                    if (nizStudenta[index].Starost < nizStudenta[i].Starost)
                    {
                        index = i;
                    }
                
            }
            return index;
        }
        static void Main(string[] args)
        {
            int broj;
            do
            {
                Console.WriteLine("Uneti ukupan broj studenata:");
                int.TryParse(Console.ReadLine(), out broj);
            } while (broj < 0 && broj > 100);
            Student[] nizStudenata = new Student[broj];

            for(int i = 0; i < nizStudenata.Length; i++)
            {
                Console.WriteLine("Ucitavanje podataka o studentu!");
                Console.WriteLine("Opcije: 1 - Brucos; 2 - Student vise godine; 3 - Student master studija");
                int.TryParse(Console.ReadLine(), out int opcije);
                switch (opcije)
                {

                    case 1:

                        Console.WriteLine("Novi Brucos!");
                        Ucitaj(out string ime, out string prezime, out float prosecnaOcena, out string brojIndeksa, out ushort godinaRodjenja);
                        Console.WriteLine("Broj bodova na prijemnom:");
                        float.TryParse(Console.ReadLine(), out float bodoviNaPrijemnom);
                        Console.WriteLine("Da li je nagradjivan? Opcije ( 0 - NE; 1 - DA)");
                        int.TryParse(Console.ReadLine(), out int price);
                        while (price < 0 || price > 1)
                        {
                            Console.WriteLine("Da li je nagradjivan? Opcije ( 0 - NE; 1 - DA)");
                            int.TryParse(Console.ReadLine(), out price);
                        }
                        switch (price)
                        {
                            case 0:

                                Nagradjivanje nagrada = Nagradjivanje.NE;
                                nizStudenata[i] = new Brucos(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja, bodoviNaPrijemnom, nagrada);
                                break;

                            case 1:
                                nagrada = Nagradjivanje.DA;
                                nizStudenata[i] = new Brucos(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja, bodoviNaPrijemnom, nagrada);
                                break;
                           
                        }
                        break;

                    case 2:

                        Console.WriteLine("Novi student vise godine!");
                        Ucitaj(out  ime, out  prezime, out  prosecnaOcena, out  brojIndeksa, out  godinaRodjenja);
                        Console.WriteLine("Broj zaostalih ispita:");
                        int.TryParse(Console.ReadLine(), out int brojZaostalihIspita);
                        Console.WriteLine("Godina studija (1 - 5):");
                        int.TryParse(Console.ReadLine(), out int godinaStudija);
                        nizStudenata[i] = new StudentViseGodine(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja, brojZaostalihIspita, godinaStudija);
                        break;

                    case 3:
                        
                        Console.WriteLine("Novi student master studija!");
                        Ucitaj(out ime, out prezime, out prosecnaOcena, out brojIndeksa, out godinaRodjenja);
                        Console.WriteLine("Broj isgubljenih godina:");
                        int.TryParse(Console.ReadLine(), out int brojIzgubljenihgodina);
                        Console.WriteLine("Smer: Opcije (1 - Racunarstvo; 2 - Automatika; 3 - Elektronika; 4 - Energetika):");
                        int.TryParse(Console.ReadLine(), out int field);
                        while(field < 1 || field > 4)
                        {
                            Console.WriteLine("Smer: Opcije (1 - Racunarstvo; 2 - Automatika; 3 - Elektronika; 4 - Energetika):");
                            int.TryParse(Console.ReadLine(), out field);
                        }
                        switch (field)
                        {
                            case 1:

                                Smer smer = Smer.Racunarstvo;
                                nizStudenata[i] = new StudentMasterStudija(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja, brojIzgubljenihgodina, smer);
                                break;
                            case 2:

                                smer = Smer.Automatika;
                                nizStudenata[i] = new StudentMasterStudija(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja, brojIzgubljenihgodina, smer);
                                break;
                            case 3:

                                smer = Smer.Elektronika;
                                nizStudenata[i] = new StudentMasterStudija(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja, brojIzgubljenihgodina, smer);
                                break;
                            case 4:

                                smer = Smer.Energetika;
                                nizStudenata[i] = new StudentMasterStudija(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja, brojIzgubljenihgodina, smer);
                                break;
                        }
                        break;

                }
            }

            foreach(Student s in nizStudenata)
            {
                s.Prikaz();
            }
            Console.WriteLine();
            int index1 = NajviseBodova(nizStudenata);
            Console.WriteLine("Student sa najvise bodova je:");
            nizStudenata[index1].Prikaz();
            Console.WriteLine();
            int index2 = NajstarijiStudent(nizStudenata);
            Console.WriteLine("Najstariji student je:");
            nizStudenata[index2].Prikaz();
        }
    }
}
