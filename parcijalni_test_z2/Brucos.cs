﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parcijalni_test_z2
{
    class Brucos : Student
    {
        private float bodoviNaPrijemnom;
        private Nagradjivanje nagrada;

        public Brucos(string ime, string prezime, float prosecnaOcena, string brojIndeksa, ushort godinaRodjenja,
        float bodoviNaPrijemnom, Nagradjivanje nagrada)

        : base(ime, prezime, prosecnaOcena, brojIndeksa, godinaRodjenja)

        {
            this.bodoviNaPrijemnom = bodoviNaPrijemnom;
            this.nagrada = nagrada;
        }


        public override float Bodovi()
        {
            if(this.nagrada == Nagradjivanje.DA)
            {
                return base.Bodovi() + 10;
            }
            else
            {
                return base.Bodovi();
            }
        }

        public override void Prikaz()
        {
            Console.WriteLine("Brucos:");
            base.Prikaz();
            Console.WriteLine($" Broj bodova na prijemnom: {this.bodoviNaPrijemnom} Nagradjivan: {this.nagrada} je osvojio {this.Bodovi()} bodova.");
        }
    }

    
}
