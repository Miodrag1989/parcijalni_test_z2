﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parcijalni_test_z2
{
    class StudentMasterStudija : Student
    {
        private int brojIzgubljenihgodina;
        private Smer smer;

        public StudentMasterStudija(string ime, string prezime, float prosecnaOcena, string brojIndeksa, ushort godinaRodjenja,
            int brojIzgubljenihgodina, Smer smer)

            :base(ime,prezime,prosecnaOcena,brojIndeksa,godinaRodjenja)
        {
            this.brojIzgubljenihgodina = brojIzgubljenihgodina;
            this.smer = smer;
        }

        public override float Bodovi()
        {
            return 20 + base.Bodovi() - (4 * this.brojIzgubljenihgodina);
        }

        public override void Prikaz()
        {
            Console.WriteLine("Student master studija:");
            base.Prikaz();
            Console.WriteLine($"Broj izgubljenih godina: {this.brojIzgubljenihgodina} Smer: {this.smer} je osvojio {this.Bodovi()} bodova.");
        }
    }
}
