﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parcijalni_test_z2
{
    class Student
    {
        private const ushort trenutnaGodina = 2019;
        protected string ime;
        protected string prezime;
        protected float prosecnaOcena;
        protected string brojIndeksa; //String zbog toga sto mogu da se jave i slova primer. FIB-13044
        protected ushort godinaRodjenja;

        public Student(string ime,string prezime,float prosecnaOcena,string brojIndeksa,ushort godinaRodjenja)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.prosecnaOcena = prosecnaOcena;
            this.brojIndeksa = brojIndeksa;
            this.godinaRodjenja = godinaRodjenja;

        }

        public virtual float Bodovi()
        {
            if(Student.trenutnaGodina - this.godinaRodjenja < 26)
            {
                return (6 * this.prosecnaOcena) + 5;
            }
            else
            {
                return 6 * this.prosecnaOcena;
            }
        }

        public virtual void Prikaz()
        {
            Console.WriteLine($"Ime: {this.ime} Prezime: {this.prezime} Prosecna ocena: {this.prosecnaOcena} Broj Indeksa: {this.brojIndeksa} Godina rodjenja: {this.godinaRodjenja}");
        }

        public string Ime
        {
            get
            {
                return this.ime;
            }
        }

        public ushort Starost
        {
            get
            {
                return (ushort)(Student.trenutnaGodina - this.godinaRodjenja);
            }
        }
    }
}
